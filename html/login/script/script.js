$(document).ready(function () {

    $("#signIn").click(function () {
        $(".container").hide();
        $(".container-signIn").show();
        $("title").text("Signin | Sroul Tinh");

    });

    $("#register").click(function () {
        $(".container-signIn").hide();
        $(".container").show();
        $("title").text("Register | Sroul Tinh");
    });

    // Submit Button Event
    $('#btn-submit').click(function () {

        //Call Function Register
        validateFormRegister();
    });

    // Function for Validate all data input
    function validateFormRegister() {

        // Using Regular Expression
        var nameReg = /^[A-Za-z]+$/;
        var spaceReg = new RegExp("[ ]","g");
        var textReg = new RegExp("[0-9]", "g");
        var passwordCharReg = new RegExp("[^a-z]", "g");
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        // declare variable to handle text field from form input type
        var username = $('#txtUsername').val();
        var password = $('#txtPassword').val();
        var confrmPassword = $('#txtConfirmPassword').val();
        var email = $('#txtEmail').val();

        // Working with conditon
        if (username == "") {
            $("#errorUsername").text("*Username must not be blank!");
        } else if (spaceReg.test(username)) {
            $("#errorUsername").text("*Username Must not Contain a Space!");
        } else if (!emailReg.test(email)) {
            $("#errorEmail").text("*Email is not Correct");
        } else if (username.length < 4) {

            $("#errorUsername").text("*Username must be more than 3 characters!");
        } else if (password.length < 4) {
            $("#errorPassword").text("*Password is too short");
        } else if (confrmPassword != password) {
            $("#errorConfirm").text("*Password you entered is not matched");
        } else {
            alert("Sigup Successfully");
            window.location.assign("../profile.html")
        }
    }

    // Fosucout Function Showing Error
    $("#txtUsername").focusout(function(){

        var textReg = new RegExp("[0-9]", "g");
        var username = $('#txtUsername').val();

        // Working with conditon
        if (username ==" ") {
            $("#errorUsername").text("*Username must not be blank!");
        } 
        else if(username.length<4)
        {
            $("#errorUsername").text("*User Name must be 4characters long");
        }
        // else if (spaceReg.test(username)) {
        //     $("#errorUsername").text("*Username Must not Contain spacing!");
        // }
    });

    // Fosucout Function Showing Error
    $("#txtEmail").focusout(function(){

        var emailReg =/[@.]/;
        var email = $('#txtEmail').val();

        // Working with conditon
        if(!emailReg.test(email))
        {
            $("#errorEmail").text("*Email is not correct");
            return false;
        }
    });

    // Fosucout Function Showing Error
    $("#txtPassword").focusout(function(){

        // var numberReg = /[0-9]+$/;
        var spaceReg = new RegExp("[ ]", "g");
        var password = $('#txtPassword').val();

        // Working with conditon
        if(spaceReg.test(password))
        {
            $("#errorPassword").text("*Password must not contain space");
        }
        else if(password<4)
        {
            $("#errorPassword").text("*Password must be 5 characters long");
        }
    });

    // Focusout Function clearing Error 
    $("input").focusout(function(){
        var nameReg = /[A-Za-z]+$/;
        var space = new RegExp("[ ]","g");
        var textReg = new RegExp("[0-9]", "g");
        var passwordCharReg = new RegExp("[a-z]", "g");
        var emailReg = /([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        var username = $('#txtUsername').val();
        var password = $('#txtPassword').val();
        var confrmPassword = $('#txtConfirmPassword').val();
        var email = $('#txtEmail').val();

        if(nameReg.test(username)){
            $("#errorUsername").text("");
        }
        if(space.test(username))
        {
            $("#errorUsername").text("");
        }
        if(emailReg.test(email))
        {
            $("#errorEmail").text("");
        }
        if(textReg.test(password))
        {
            $("#errorPassword").text("");
        }
        if(password==confrmPassword)
        {
            $("#errorConfirm").text("");
        }
    });
        
    // Onchange Event
    $("#btnSignin").click(function () {
        
        var profileLink = "User Profile.html";
        var username = $('#test').val();
        var password = $('#test1').val();
        if ("phanith" == username) {   
        }
        if(password=="12345") 
        {
            // alert("login");
            alert("Sigin Successfully");
            window.location.assign("../profile.html")
            // $("#btnSignin").append("<a href='../profile.html'></a>")
        }
        else{
            $("#errorSignin").text("Default user phanith and Password 12345!");
            $("#errorSignin").css("border", "1px solid");
        }
    });
});
// Submit Button Event